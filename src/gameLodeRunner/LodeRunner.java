package gameLodeRunner;

import javax.swing.JFrame;
import javax.swing.JButton;

import sessionLodeRunner.*;

import java.awt.BorderLayout;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

/**
 * Die Main-Klasse des Spiels LodeRunner.
 * 
 * @author Benjamin Erdnuess und Adrian Ortenburger
 */
public class LodeRunner implements GameSessionListener {
	/**
	 * Initialisiert nur eine Instanz der eigenen Klasse.
	 */
	public static void main(String[] args) {
		new LodeRunner();
	}

	private JButton btnExit, btnLvl1, btnLvl2;
	private JFrame frame;

	private WindowListener gameWindowListener = new WindowListener() {

		@Override
		public void windowActivated(WindowEvent e) {
			System.out.println("windowActivated");
			if (session != null) {
				try {
					Thread.sleep(500);
				} catch (InterruptedException e1) {
					e1.printStackTrace();
				}
				session.resume();
				session.window.requestFocus();
			}
		}

		@Override
		public void windowClosed(WindowEvent e) {
		}

		@Override
		public void windowClosing(WindowEvent e) {
		}

		@Override
		public void windowDeactivated(WindowEvent e) {
			System.out.println("windowDeactivated");
			session.pause();
		}

		@Override
		public void windowDeiconified(WindowEvent e) {
		}

		@Override
		public void windowIconified(WindowEvent e) {
		}

		@Override
		public void windowOpened(WindowEvent e) {
		}
	};
	public GameSession session;

	/**
	 * Initialisiert das Spiel.
	 */
	public LodeRunner() {
		frame = new JFrame();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setBounds(100, 100, 450, 300);
		frame.setResizable(false);
		frame.setUndecorated(true);

		btnLvl1 = new JButton("Level 1");
		btnLvl1.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (session == null) {
					initializeSession(1);
				}
			}
		});
		frame.getContentPane().add(btnLvl1, BorderLayout.WEST);

		btnLvl2 = new JButton("Level 2");
		btnLvl2.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (session == null) {
					initializeSession(2);
				}
			}
		});
		frame.getContentPane().add(btnLvl2, BorderLayout.EAST);

		btnExit = new JButton("Exit");
		btnExit.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				System.exit(0);
			}
		});
		frame.getContentPane().add(btnExit, BorderLayout.SOUTH);

		frame.setVisible(true);
	}

	/**
	 * Aktiviert das Spielfenster, davor sollte die Session initialisiert sein.
	 */
	private void activateGameWindow() {
		frame.getContentPane().remove(btnLvl1);
		frame.getContentPane().remove(btnLvl2);
		frame.setTitle("Lode Runner - " + session.level.getName());
		frame.setSize(session.level.getWidth() * GridAddress.GRID_SIZE,
				session.level.getHeight() * GridAddress.GRID_SIZE);
		frame.addWindowListener(gameWindowListener);
		frame.getContentPane().add(session.window);
		session.window.requestFocus();
	}

	/**
	 * Deaktiviert das Spielfenster.
	 */
	private void deactivateGameWindow() {
		frame.setBounds(100, 100, 450, 300);
		frame.getContentPane().add(btnLvl1, BorderLayout.WEST);
		frame.getContentPane().add(btnLvl2, BorderLayout.EAST);
		frame.removeWindowListener(gameWindowListener);
		frame.getContentPane().remove(session.window);
	}

	/**
	 * Initialisiert ein Level des Spiels.
	 */
	private void initializeSession(int levelNumber) {
		session = new GameSession("levelfolder/Level " + levelNumber);
		activateGameWindow();
		// Das Observerpattern ist quasi sinnlos, weil dies der einzige
		// GameSessionListeners des Programms ist. huehue
		session.addGameSessionListener(this);
		session.startSession();
	}

	/**
	 * Beendet das Spiel, entlaedt die Session und kehrt zum Startfenster
	 * zurueck.
	 */
	@Override
	public void levelPassed() {
		session.end();
		deactivateGameWindow();
		session = null;
	}

	/**
	 * Started das Level neu.
	 */
	@Override
	public void playerDied() {
		session.end();
		deactivateGameWindow();
		session = new GameSession(session.level.getLevelPath());
		activateGameWindow();
		session.addGameSessionListener(this);
		session.startSession();
	}
}