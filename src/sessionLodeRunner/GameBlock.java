package sessionLodeRunner;

import java.awt.image.BufferedImage;

/**
 * Ein Block im Spiel, es enthaelt alle Werkzeuge zum setzen der ID des Blocks,
 * sowie der Bilder. Zusätzlich finden sich hier Funktionen zum Graben eines
 * Blocks.
 */
public class GameBlock {
	public static final byte ID_BRICK = 2;

	public static final byte ID_CLAY = 3;
	public static final byte ID_EMPTY = 0;
	public static final byte ID_HOLE = 4;

	public static final byte ID_LADDER = 5;

	public static final byte ID_ROD = 6;
	public static final byte ID_TREASURE = 1;

	/** Zeit in Sekunden, die benötigt wird ein Loch zu graben. */
	public static final double TIME_DIG_HOLE = 0.80;
	/** Zeit in Sekunden, bis ein gegrabenes Loch wieder geschlossen ist. */
	public static final double TIME_FILL_HOLE = 8;

	private boolean blockedByEnemie;
	private byte blockID;
	private double clayHoleConversionFactor;
	private BufferedImage imageForeground;
	private BufferedImage imageMain;

	public GameBlock(byte id) {
		blockID = id;
		clayHoleConversionFactor = 1;
	}

	/**
	 * Es sollte {@link Level#reAssign(GridAddress, byte)} statt dieser Funktion
	 * aufgerufen werden, damit auch die Texturen waehrend der Runtime geändert
	 * werden.
	 */
	public void changeID(byte id) {
		blockID = id;
	}

	/**
	 * @param deltaTime
	 *            Vergangene Zeit seit dem letzten Game-Tick.
	 * @return True, wenn das Loch vollständig gegraben wurde.
	 */
	public boolean dickHole(double deltaTime) {
		double digFactor = (TIME_FILL_HOLE * TIME_DIG_HOLE)
				/ (TIME_FILL_HOLE + TIME_DIG_HOLE);
		clayHoleConversionFactor -= deltaTime / digFactor;
		updateClayHole();
		if (clayHoleConversionFactor <= 0) {
			clayHoleConversionFactor = 0;
			return false;
		} else {
			return true;
		}
	}

	/**
	 * @param deltaTime
	 *            Vergangene Zeit seit dem letzten Game-Tick.
	 */
	public void fillUpHole(double deltaTime) {
		clayHoleConversionFactor += deltaTime / TIME_FILL_HOLE;
		if (clayHoleConversionFactor >= 1) {
			clayHoleConversionFactor = 1;
		}
		updateClayHole();
	}

	public void setBlockedByEnemie(boolean blocked) {
		blockedByEnemie = blocked;
	}

	public boolean isBlockedByEnemie() {
		return blockedByEnemie;
	}

	/**
	 * @return Eine Zahl zwischen 0 und 1, welche den Übergang zwischen Loch und
	 *         Erde darstellt.
	 */
	public double getClayHoleConversionFactor() {
		return clayHoleConversionFactor;
	}

	public BufferedImage getForeground() {
		return imageForeground;
	}

	public byte getID() {
		return blockID;
	}

	public BufferedImage getImage() {
		return imageMain;
	}

	public boolean isClay() {
		return (blockID == ID_CLAY);
	}

	/**
	 * @return Würde ein Spieler durch diesen Block fallen?
	 */
	public boolean isFallable() {
		return (blockID == ID_EMPTY || blockID == ID_TREASURE
				|| (blockID == ID_HOLE && !blockedByEnemie) || blockID == ID_ROD);
	}

	public boolean isHole() {
		return (blockID == ID_HOLE);
	}

	public boolean isLadder() {
		return (blockID == ID_LADDER);
	}

	public boolean isRod() {
		return (blockID == ID_ROD);
	}

	/**
	 * @return Kann man durch diesen Block laufen?
	 */
	public boolean isSolid() {
		return (blockID == ID_CLAY || blockID == ID_BRICK);
	}

	public boolean isTreasure() {
		return (blockID == ID_TREASURE);
	}

	/**
	 * @return Benoetigt der Block einen Hintergrund?
	 */
	public boolean isWithinAir() {
		return (blockID == ID_EMPTY || blockID == ID_TREASURE
				|| blockID == ID_LADDER || blockID == ID_ROD);
	}

	public void setForeground(BufferedImage image) {
		imageForeground = image;
	}

	public void setImage(BufferedImage image) {
		this.imageMain = image;
	}

	private void updateClayHole() {
		if (blockID == ID_CLAY && clayHoleConversionFactor <= 0) {
			blockID = ID_HOLE;
		}
		if (blockID == ID_HOLE && clayHoleConversionFactor >= 1) {
			blockID = ID_CLAY;
			blockedByEnemie = false;
		}
	}
}
