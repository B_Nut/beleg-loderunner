package sessionLodeRunner;

import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;

/**
 * Das Fenster, welches das Spiel darstellt. Und aktualisierungen durchfuehrt.
 */
public class GameWindow extends Canvas {

	private static final long serialVersionUID = -1103976221164682867L;

	private GameSession session;

	public GameWindow(GameSession session) {
		this.session = session;
	}

	public void updateWindow() {
		BufferStrategy bs = getBufferStrategy();
		if (bs == null) {
			createBufferStrategy(3);
			bs = getBufferStrategy();
		}

		Graphics g = bs.getDrawGraphics();

		// iterate through the 2d gridarray line per line
		for (int yi = 0; yi < session.level.getHeight(); yi++) {
			for (int xi = 0; xi < session.level.getWidth(); xi++) {
				// Draw the images in the grid.
				GameBlock gB = session.level.getBlock(new GridAddress(xi, yi));
				g.drawImage(gB.getImage(), xi * GridAddress.GRID_SIZE, yi
						* GridAddress.GRID_SIZE, GridAddress.GRID_SIZE,
						GridAddress.GRID_SIZE, null);
			}
		}

		refreshLodePerson(g, TextureContainer.PLAYER_IDLE, session.player);
		for (LodePerson enemie : session.enemies) {
			refreshLodePerson(g, TextureContainer.ENEMIE_IDLE, enemie);
		}

		for (int yi = 0; yi < session.level.getHeight(); yi++) {
			for (int xi = 0; xi < session.level.getWidth(); xi++) {
				// Draw the images in the grid.
				GameBlock gB = session.level.getBlock(new GridAddress(xi, yi));
				g.drawImage(gB.getForeground(), xi * GridAddress.GRID_SIZE, yi
						* GridAddress.GRID_SIZE, GridAddress.GRID_SIZE,
						GridAddress.GRID_SIZE, null);
			}
		}

		g.dispose();
		bs.show();
	}

	@Override
	public void paint(Graphics g) {
		if (this.getBufferStrategy() != null) {
			this.getBufferStrategy().show();
		}
	}

	private void refreshLodePerson(Graphics g, BufferedImage image, LodePerson lP) {
		if (lP.getLookingDirection()) {
			g.drawImage(image, (int) lP.getPositionX(),
					(int) lP.getPositionY(), GridAddress.GRID_SIZE,
					GridAddress.GRID_SIZE, null);
		} else {
			g.drawImage(image, (int) lP.getPositionX() + GridAddress.GRID_SIZE,
					(int) lP.getPositionY(), -GridAddress.GRID_SIZE,
					GridAddress.GRID_SIZE, null);
		}
	}
}
