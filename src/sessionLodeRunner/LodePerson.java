package sessionLodeRunner;

/**
 * Die Elternklasse fuer {@link Player} und {@link Enemie}.
 */
public class LodePerson {
		
	private GridAddress spawnAddress;
	
	// CKC steht fuer CustomKeyCode
	public static final int CKC_DIG_LEFT = 5;
	public static final int CKC_DIG_RIGHT = 6;
	public static final int CKC_DOWN = 4;
	public static final int CKC_LEFT = 1;
	public static final int CKC_NOTHING = 0;
	public static final int CKC_RIGHT = 2;
	public static final int CKC_UP = 3;
	public static final int CLIMBING_SPEED = GridAddress.GRID_SIZE * 3;
	public static final int WALKING_SPEED = GridAddress.GRID_SIZE * 3; // Grids per
																// Second
	// Die Möglichen Stati des Spielers.
	private boolean climbing;
	private boolean climbingDirection; // true => up; false => down
	private boolean digging;
	private boolean diggingDirection;
	private boolean walking;
	private boolean lookingDirection; // true => right; false => left
	private boolean falling; // Es gibt nur eine Fallrichtung. :3

	// Die Position des Spielers in Px.
	private double positionX;
	private double positionY;

	// Speichert die Momentane Richtung des Spielers.
	private int wantedDirectionX;
	private int wantedDirectionY;
	private int wantedDirectionDig;
	
	private LodePersonInput input;

	// Zwischenspeicher für den aktuellen Wunsch des Spielers.
	protected boolean wantsToClimb;
	protected boolean wantsToDig;
	protected boolean wantsToWalk;

	/**
	 * @param gA
	 *            Die Spawnadresse des Teilnehmers.
	 */
	public LodePerson(GridAddress gA) {
		spawnAddress = gA;
		reset();
	}
	
	public void reset(){
		
		positionX = spawnAddress.getPixelX();
		positionY = spawnAddress.getPixelY();

		lookingDirection = true;
		climbingDirection = false;

		walking = true;
		climbing = false;
		falling = false;
		digging = false;

		wantsToClimb = false;
		wantsToWalk = false;
		wantsToDig = false;
	}
	
	/**
	 * @return Die Adresse des Blocks, den der Spieler als naechstes ausfuellen
	 *         wuerde, wuerde er seine Richtung beibehalten.
	 */
	public GridAddress faceBlock() {
		GridAddress returnAddress = null;
		if (walking) {
			if (lookingDirection) {
				returnAddress = GridAddress.pixelToGridAddress((int) positionX
						+ GridAddress.GRID_SIZE - 1, (int) positionY);
			} else {
				returnAddress = GridAddress.pixelToGridAddress((int) positionX,
						(int) positionY);
			}
		} else if (climbing || falling) {
			if (climbingDirection) {
				returnAddress = GridAddress.pixelToGridAddress((int) positionX,
						(int) positionY);
			} else {
				returnAddress = GridAddress.pixelToGridAddress((int) positionX,
						(int) positionY + GridAddress.GRID_SIZE - 1);
			}
		} else {
			returnAddress = GridAddress.pixelToGridAddress((int) positionX,
					(int) positionY);
		}
		return returnAddress;
	}
	
	public int getWantedDirectionX(){
		return wantedDirectionX;
	}
	public int getWantedDirectionY(){
		return wantedDirectionY;
	}
	public int getWantedDirectionDig(){
		return wantedDirectionDig;
	}
	public boolean getClimbingDirection() {
		return climbingDirection;
	}

	public boolean getDiggingDirection() {
		return diggingDirection;
	}

	/**
	 * Holt den CustomKeyCode aus {@link LodePersonInput#getInput()} und uebersetzt ihn in lokale Variablen.
	 */
	public void translateInput(){
		// Da die Abfrage der Richtung von einem anderen Thread gesteuert wird
		// (KeyListener) ist es wichtig diesen Wert an nur einem Punkt
		// abzufragen.
		int input = this.input.getInput();
		switch (input) {
		case CKC_LEFT:
			setWantedWalkingDirection(-1);
			break;
		case CKC_RIGHT:
			setWantedWalkingDirection(1);
			break;
		case CKC_UP:
			setWantedClimbingDirection(-1);
			break;
		case CKC_DOWN:
			setWantedClimbingDirection(1);
			break;
		case CKC_DIG_LEFT:
			wantedDirectionDig = -1;
			break;
		case CKC_DIG_RIGHT:
			wantedDirectionDig = 1;
			break;
		}
	}
	
	public boolean getLookingDirection() {
		return lookingDirection;
	}

	/**
	 * @return Der Pixel in X-Richtung, den die linke obere Ecke der Person
	 *         darstellt.
	 */
	public double getPositionX() {
		return positionX;
	}

	/**
	 * @return Der Pixel in Y-Richtung, den die linke obere Ecke der Person
	 *         darstellt.
	 */
	public double getPositionY() {
		return positionY;
	}
	public GridAddress getSpawnAddress(){
		return spawnAddress;
	}
	public boolean isClimbing() {
		return climbing;
	}

	public boolean isDigging() {
		return digging;
	}

	public boolean isFalling() {
		return falling;
	}

	public boolean isWalking() {
		return walking;
	}
	
	public void setLodePersonInput(LodePersonInput input){
		this.input = input;
	}

	public void setClimbingDirection(boolean climbingDirection) {
		this.climbingDirection = climbingDirection;
	}

	public void setWantedClimbingDirection(int direction) {
		wantedDirectionY = direction;
	}

	public void setDiggingDirection(boolean diggingDirection) {
		this.diggingDirection = diggingDirection;
	}

	public void setFalling(boolean isFalling) {
		falling = isFalling;
		climbingDirection = false;
	}

	public void setLookingDirection(boolean direction) {
		lookingDirection = direction;
	}

	public void setOnLadder(boolean onLadder) {
		this.climbing = onLadder;
	}

	public void setPosition(double positionX, double positionY) {
		this.positionX = positionX;
		this.positionY = positionY;
	}

	public void setWalking(boolean walking) {
		this.walking = walking;
	}

	public void setWantedWalkingDirection(int direction) {
		wantedDirectionX = direction;
	}

	public void startDigging() {
		digging = true;
		diggingDirection = wantedDirectionDig == 1;
	}

	public void stop() {
		wantedDirectionX = 0;
		wantedDirectionY = 0;
	}

	public void stopClimbing() {
		wantedDirectionY = 0;
	}

	public void stopDigging() {
		digging = false;
		wantedDirectionDig = 0;
		wantsToDig = false;
		setWalking(true);
	}

	public void stopWalking() {
		wantedDirectionX = 0;
	}
	
	/**
	 * Errechnet, ob der Spieler beim Graben den Block direkt vor sich, oder den nachfolgenden Block meint.
	 * @param digDirection die Richtung, in die gedrückt wurde.
	 * @return die Adresse über dem Block, der abgegraben werden soll.
	 */
	public GridAddress whereToDig(boolean digDirection) {
		double multiplier = 0;
		if (walking) {
			if (digDirection) {
				multiplier = 1.5;
			} else {
				multiplier = -0.5;
			}
		}
		return GridAddress.pixelToGridAddress((int) positionX
				+ (int) (multiplier * GridAddress.GRID_SIZE), (int) positionY);
	}
}