package sessionLodeRunner;

/**
 * Stellt die Verbindung zwischen der GridMap und den eigentlichen Pixeln her.
 */
public class GridAddress {
	public final static int GRID_SIZE = 32;

	/**
	 * Ist der abgefragte Pixel ausserhalb des Levels, gibt diese Funktion
	 * trotzdem eine gültige Adresse zurueck.
	 * 
	 * @return Die theoretische Adresse des Grids in dem sich der gegebene Pixel
	 *         befindet.
	 */
	public static GridAddress pixelToGridAddress(int pixelX, int pixelY) {
		return new GridAddress((int) (pixelX / GRID_SIZE),
				(int) (pixelY / GRID_SIZE));
	}

	private int x;
	private int y;

	public GridAddress(int x, int y) {
		this.x = x;
		this.y = y;
	}

	public boolean equals(GridAddress other) {
		if (this.x == other.x && this.y == other.y) {
			return true;
		} else {
			return false;
		}
	}

	public int getPixelX() {
		return x * GRID_SIZE;
	}

	public int getPixelY() {
		return y * GRID_SIZE;
	}

	public int getX() {
		return x;
	}

	public int getY() {
		return y;
	}

	public GridAddress offset(int x, int y) {
		return new GridAddress(this.x + x, this.y + y);
	}

	@Override
	public String toString() {
		return ("(" + x + ":" + y + ")");
	}
}