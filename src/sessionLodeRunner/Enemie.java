package sessionLodeRunner;

/**
 * Die Gegner im Spiel.
 */
public class Enemie extends LodePerson{
	
	protected boolean isHoldingTreasure;
	
	public Enemie(GridAddress gA) {
		super(gA);
		isHoldingTreasure = false;
		setLodePersonInput(new EnemieInput());
	}
}
