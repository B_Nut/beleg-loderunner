package sessionLodeRunner;

/**
 * Das Herz des Spiels! <br>
 * Diese Klasse sorgt dafuer, dass ein Thread nicht auf maximaler Kapazitaet
 * laeuft, sondern hinreichend genau mit einer Festgelegten Framerate aufgerufen
 * wird.
 */
public abstract class VariableTimestepRunnable implements Runnable {
	
	private static final Object PAUSED_MONITOR = new Object();
	
	private boolean isPaused;
	private boolean sessionDone;

	private final long targetTimestep;
	private long updateTime;

	private long fps;
	private long lastFpsTime;
	private long lastLoopTime;
	private long now;

	private double deltaTime;
	
	/**
	 * @param targetFPS
	 *            Die gewuenschten Frames pro Sekunde.
	 */
	public VariableTimestepRunnable(int targetFPS) {
		targetTimestep = 1000000000 / targetFPS;

		sessionDone = false;
		isPaused = false;
	}
	
	/** Beendet den Thread nach Ablauf des aktuellen Ticks. */
	public void end() {
		sessionDone = true;
	}
	
	/** Gibt die Zeit (in Sekunden) zwischen dem letzten und dem aktuellen Tick zurück. */
	public double getDeltaTime() {
		return deltaTime;
	}
	
	/** Pausiert den Thread bei naechster Gelegenheit. */
	public void pause() {
		isPaused = true;
	}

	@SuppressWarnings("unused")
	private void printFPS(long updateTime) {
		// update the frame counter
		lastFpsTime += updateTime;
		fps++;

		// update our FPS counter if a second has passed since we last recorded
		if (lastFpsTime >= 1000000000) {
			System.out.println("(" + Thread.currentThread().getName()
					+ " - FPS: " + fps + ")");
			lastFpsTime = 0;
			fps = 0;

		}
	}
	
	/** Faehrt den Thread fort. */
	public void resume() {
		isPaused = false;
		synchronized (PAUSED_MONITOR) {
			PAUSED_MONITOR.notifyAll();
		}
	}

	@Override
	public void run() {
		lastLoopTime = System.nanoTime();

		// keep looping around until the game ends
		while (!sessionDone) {

			// Determine the bygone time since the last loop.
			now = System.nanoTime();
			updateTime = now - lastLoopTime;
			lastLoopTime = now;
			
			//For development-purpose...
			//printFPS(updateTime);

			// Manage to pause the game at any time
			while (isPaused) {
				synchronized (PAUSED_MONITOR) {
					try {
						PAUSED_MONITOR.wait();
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
				// After we slept an unknown amount of time, we have to reset
				// the lastLoopTime to avoid an insane updateLenght
				lastLoopTime = System.nanoTime();
			}

			deltaTime = updateTime / (double) 1000000000;
			update();

			long restTime = lastLoopTime + targetTimestep - System.nanoTime();
			if (restTime > 0) {
				try {
					Thread.sleep(restTime / 1000000);// sleep is in ms, while
														// restTime is in ns
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}
	
	/** 
	 * Die Update Funktion wird hinreichend genau nach der gewuenschen FPS-Zahl aufgerufen- <br>
	 * Zeitabhaengige Dinge - wie Geschwindigkeiten - koennen mit {@link #getDeltaTime()} berechnet werden.
	 */
	protected abstract void update();
}
