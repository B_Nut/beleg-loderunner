package sessionLodeRunner;

/**
 * Input input input input!
 */
public interface LodePersonInput {
	/**
	 * Soll die Aktuellste Eingabe der Quelle zur�ck geben.
	 * @return CustomKeyCode, wie in {@link LodePerson} definiert.
	 */
	public int getInput();
}
