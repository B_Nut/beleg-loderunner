package sessionLodeRunner;

/**
 * Diese Klasse enthaelt alle Regeln des Spiels in mehr oder minder, aber mehr
 * minder lesbarer Form. Hauptsache es funktioniert. Sorry, aber wir hatten dann
 * nicht so viel Zeit.
 */
public class GameLogic extends VariableTimestepRunnable {

	private GameSession session;

	public GameLogic(GameSession gS, int targetFPS) {
		super(targetFPS);
		session = gS;
	}

	private void checkForDeath(Player player) {
		GameBlock playerBlock = session.level.getBlock(GridAddress.pixelToGridAddress(
				(int) player.getPositionX(), (int) player.getPositionY()));
		if (playerBlock != null && playerBlock.isSolid()) {
			dyingBehaviour();
		}
	}

	private void checkForDeath(Enemie enemie) {
		if (Math.abs(enemie.getPositionX() - session.player.getPositionX()) < GridAddress.GRID_SIZE
				&& Math.abs(enemie.getPositionY() - session.player.getPositionY()) < GridAddress.GRID_SIZE) {
			dyingBehaviour();
			return;
		}

		GridAddress enemieAddress = GridAddress.pixelToGridAddress(
				(int) enemie.getPositionX(), (int) enemie.getPositionY());
		if (session.level.getBlock(enemieAddress) == null
				|| session.level.getBlock(enemieAddress).isSolid()) {
			enemie.reset();
		}
	}

	private void checkForBlockedHoles(Enemie e) {
		GameBlock enemieBlock = session.level.getBlock(e.faceBlock());
		if (enemieBlock.isHole()) {
			enemieBlock.setBlockedByEnemie(true);
		}
	}

	private void checkTreasures() {
		if (!session.player.isDigging() && session.level.getBlock(session.player.faceBlock()) != null
				&& session.level.getBlock(session.player.faceBlock()).isTreasure()) {
			session.level.eliminateTreasure(session.player.faceBlock());
		}
		for (Enemie e : session.enemies) {
			if (session.level.getBlock(e.faceBlock()).isTreasure()
					&& !e.isHoldingTreasure) {
				session.level.justTakeTreasure(e.faceBlock());
				e.isHoldingTreasure = true;
			}
		}
		if (session.level.getNumberOfTreasures() == 0) {
			session.level.openLevelExit();
		}
	}

	private void dyingBehaviour() {
		session.castPlayerDied();
	}

	private void fillUpHoles() {
		for (int y = 0; y < session.level.getHeight(); y++) {
			for (int x = 0; x < session.level.getWidth(); x++) {
				GameBlock currentBlock = session.level.getBlock(new GridAddress(x, y));
				if (currentBlock.getClayHoleConversionFactor() < 1) {
					currentBlock.fillUpHole(getDeltaTime());
					session.level.reAssignTexture(new GridAddress(x, y));
					session.level.reAssignTexture(new GridAddress(x + 1, y + 0));
					session.level.reAssignTexture(new GridAddress(x - 1, y + 0));
				}
			}
		}
	}

	private void interpretLodePersonInput(LodePerson lodePerson) {
		GridAddress personGrid;
		GridAddress newPersonGrid;

		double personPosX = lodePerson.getPositionX();
		double personPosY = lodePerson.getPositionY();
		personGrid = GridAddress.pixelToGridAddress((int) personPosX,
				(int) personPosY);

		int wantedDigrection = lodePerson.getWantedDirectionDig();
		int wantedClirection = lodePerson.getWantedDirectionY();
		int wantedDirection = lodePerson.getWantedDirectionX();

		boolean digDirection = wantedDigrection == 1;

		int clirection = 0;
		int direction = 0;

		GridAddress faceBlock = lodePerson.faceBlock();

		// _________Nimmt die aktuellen Spielereingaben, prüft diese auf
		// Machbarkeit und plant den nächsten Schritt._________
		if (lodePerson.isFalling()) {
			if (wantedDirection != 0) {
				lodePerson.wantsToWalk = true;
				lodePerson.wantsToClimb = false;
				lodePerson.stopClimbing();
				lodePerson.setLookingDirection(wantedDirection == 1);
			}
			if (wantedClirection == 1) {
				lodePerson.wantsToClimb = true;
				lodePerson.wantsToWalk = false;
				lodePerson.stopWalking();
			}
			if (wantedClirection == -1) {
				lodePerson.wantsToClimb = false;
				lodePerson.wantsToWalk = false;
				lodePerson.stopClimbing();
				lodePerson.stopWalking();
			}
			if (wantedDigrection != 0) {
				lodePerson.stopDigging();
			}
		} else if (lodePerson.isWalking()) {
			direction = wantedDirection;
			if (wantedDigrection != 0) {
				GridAddress whereToDig = lodePerson.whereToDig(digDirection);
				if (!session.level.getBlock(whereToDig).isSolid()
						&& !session.level.getBlock(whereToDig).isTreasure()
						&& session.level.getBlock(whereToDig.offset(0, 1)).isClay()
						&& !session.level.getBlock(
								whereToDig.offset(-wantedDigrection, 1))
								.isFallable()) {
					lodePerson.wantsToDig = true;
					wantedClirection = 0;
					lodePerson.wantsToClimb = false;
					// Ok, now I know that I want to dig and I am allowed to
					// dig, in what Direction do I have to walk?
					if ((int) lodePerson.getPositionX() == personGrid
							.getPixelX()) {
						direction = 0;
					} else {
						if (digDirection) {
							if (personGrid.offset(1, 0).equals(whereToDig)) {
								direction = -1;
							} else {
								direction = 1;
							}
						} else {
							if (personGrid.equals(whereToDig)) {
								direction = 1;
							} else {
								direction = -1;
							}
						}
					}
				} else {
					lodePerson.stopDigging();
				}
			}
			if (wantedClirection == -1
					&& (session.level.getBlock(faceBlock).isLadder() || (session.level
							.getBlock(faceBlock).isRod() && session.level.getBlock(
							faceBlock.offset(0, -1)).isLadder()))) {
				lodePerson.wantsToClimb = true;
			} else if (wantedClirection == 1
					&& (session.level.getBlock(faceBlock.offset(0, 1)).isLadder()
							|| (session.level.getBlock(faceBlock).isLadder() && session.level
									.getBlock(faceBlock.offset(0, 1))
									.isFallable()) || (session.level
							.getBlock(faceBlock).isRod() && session.level.getBlock(
							faceBlock.offset(0, 1)).isFallable()))) {
				lodePerson.wantsToClimb = true;
			} else if (wantedClirection != 0) {
				lodePerson.stop();
				lodePerson.wantsToClimb = false;
			}
		} else if (lodePerson.isClimbing()) {
			clirection = wantedClirection;
			lodePerson.setClimbingDirection(wantedClirection == -1);
			if (wantedDirection != 0
					&& session.level.getBlock(faceBlock.offset(wantedDirection, 0))
							.isSolid()) {
				lodePerson.wantsToWalk = false;
				lodePerson.stop();
			} else if (wantedDirection != 0) {
				lodePerson.wantsToWalk = true;
			}
		}

		// ________Berechnet die theoretische naechste Position des
		// Spielers.________
		if (lodePerson.isFalling()) {
			personPosY += LodePerson.CLIMBING_SPEED * getDeltaTime();
		} else if (!lodePerson.isDigging()) {
			personPosX += LodePerson.WALKING_SPEED * getDeltaTime() * direction;
			personPosY += LodePerson.CLIMBING_SPEED * getDeltaTime()
					* clirection;
		}

		// _______Prüft, wo der Spieler theoretisch waere, korrigiert evtl. die
		// Position und aendert ggf. den Spielerstatus._____
		newPersonGrid = GridAddress.pixelToGridAddress((int) personPosX,
				(int) personPosY);

		if (lodePerson.isDigging()) {
			GridAddress digBlock;
			if (lodePerson.getDiggingDirection()) {
				digBlock = newPersonGrid.offset(1, 0);
			} else {
				digBlock = newPersonGrid.offset(-1, 0);
			}
			if (!session.level.getBlock(digBlock.offset(0, 1)).dickHole(getDeltaTime())) {
				lodePerson.stopDigging();
			}

			session.level.reAssignTexture(digBlock.offset(0, 1));
			session.level.reAssignTexture(digBlock.offset(1, 1));
			session.level.reAssignTexture(digBlock.offset(-1, 1));
			session.level.reAssignTexture(digBlock);
		}

		if (lodePerson.isFalling()) {
			if (!newPersonGrid.equals(personGrid)) {
				if (session.level.getBlock(newPersonGrid.offset(0, 1)).isSolid()
						|| session.level.getBlock(newPersonGrid.offset(0, 1))
								.isLadder()
						|| session.level.getBlock(newPersonGrid).isRod()) {
					lodePerson.setFalling(false);

					if (lodePerson.wantsToWalk) {
						personPosY = newPersonGrid.getPixelY();
						lodePerson.wantsToWalk = false;
						lodePerson.setWalking(true);
					} else if (lodePerson.wantsToClimb
							&& !session.level.getBlock(newPersonGrid.offset(0, 1))
									.isSolid()) {
						lodePerson.wantsToClimb = false;
						lodePerson.setOnLadder(true);
					} else {
						personPosY = newPersonGrid.getPixelY();
						lodePerson.wantsToClimb = false;
						lodePerson.stop();
						lodePerson.setWalking(true);
					}
				}
				if (lodePerson instanceof Enemie
						&& session.level.getBlock(newPersonGrid).isHole()) {
					personPosY = newPersonGrid.getPixelY();
					lodePerson.setFalling(false);
					lodePerson.wantsToWalk = false;
					lodePerson.wantsToClimb = false;
					lodePerson.setWalking(false);
					lodePerson.setOnLadder(false);
					Enemie e = (Enemie) lodePerson;
					if (e.isHoldingTreasure) {
						session.level.reAssign(newPersonGrid.offset(0, -1),
								GameBlock.ID_TREASURE);
					}
				}
			}
		}

		if (lodePerson.isWalking()) {
			GridAddress newBlock = null;
			GridAddress oldBlock = null;
			if (direction == 1) {
				lodePerson.setLookingDirection(true);
				newBlock = newPersonGrid.offset(1, 0);
				oldBlock = newPersonGrid;
			} else if (direction == -1) {
				lodePerson.setLookingDirection(false);
				newBlock = newPersonGrid;
				oldBlock = personGrid;
			}

			if (direction != 0) {
				if (session.level.getBlock(newBlock).isSolid()
						&& !lodePerson.wantsToClimb) {
					personPosX = oldBlock.getPixelX();
					lodePerson.stop();
				}
				if (!newPersonGrid.equals(personGrid)) {
					if (lodePerson.wantsToDig) {
						lodePerson.stopWalking();
						personPosX = oldBlock.getPixelX();
						lodePerson.setLookingDirection(digDirection);
						lodePerson.startDigging();
						lodePerson.setWalking(false);
					}
					if (lodePerson.wantsToClimb) {
						if (wantedClirection == -1) {
							lodePerson.stopWalking();
							personPosX = oldBlock.getPixelX();
							lodePerson.setWalking(false);
							lodePerson.setOnLadder(true);
							lodePerson.setClimbingDirection(true);
							lodePerson.wantsToClimb = false;
						}
						if (wantedClirection == 1) {
							lodePerson.stopWalking();
							personPosX = oldBlock.getPixelX();
							lodePerson.setWalking(false);
							lodePerson.setOnLadder(true);
							lodePerson.setClimbingDirection(false);
							lodePerson.wantsToClimb = false;
						}
					}
					if (session.level.getBlock(oldBlock).isFallable()
							&& session.level.getBlock(oldBlock.offset(0, 1))
									.isFallable()) {
						if (!session.level.getBlock(oldBlock).isRod()) {
							lodePerson.setFalling(true);
							lodePerson.setWalking(false);
							lodePerson.setOnLadder(false);
							lodePerson.stopWalking();
							personPosX = oldBlock.getPixelX();
						}
					}
				}
			}

			if (direction == 0) {
				if (lodePerson.wantsToDig) {
					lodePerson.stopWalking();
					lodePerson.setLookingDirection(digDirection);
					lodePerson.startDigging();
					lodePerson.setWalking(false);
				}
				if (lodePerson.wantsToClimb) {
					if (faceBlock.getX() > personGrid.getX()) {
						lodePerson.setWantedWalkingDirection(1);
					} else if (faceBlock.getX() == personGrid.getX()) {
						if (personGrid.getPixelX() == (int) lodePerson
								.getPositionX()) {
							lodePerson.stopWalking();
							lodePerson.setWalking(false);
							lodePerson.setOnLadder(true);
							lodePerson.wantsToClimb = false;
						} else {
							lodePerson.setWantedWalkingDirection(-1);
						}
					}
				}
			}
		}
		if (lodePerson.isClimbing()) {
			if (clirection == 1) {
				if (session.level.getBlock(newPersonGrid.offset(0, 1)).isSolid()
						&& !lodePerson.wantsToWalk) {
					personPosY = newPersonGrid.getPixelY();
					lodePerson.stop();
				}
				if (session.level.getBlock(personGrid).isRod()
						&& session.level.getBlock(personGrid.offset(0, 1)).isFallable()) {
					lodePerson.setOnLadder(false);
					lodePerson.setWalking(false);
					lodePerson.setFalling(true);
				}
				if (!newPersonGrid.equals(personGrid)) {
					if (session.level.getBlock(newPersonGrid.offset(0, 1)).isSolid()
							&& !lodePerson.wantsToWalk) {
						personPosY = newPersonGrid.getPixelY();
						lodePerson.stop();
						lodePerson.setOnLadder(false);
						lodePerson.setWalking(true);
					}
					if (lodePerson.wantsToWalk) {
						lodePerson.stopClimbing();
						personPosY = newPersonGrid.getPixelY();
						lodePerson.setOnLadder(false);
						lodePerson.setWalking(true);
						lodePerson.wantsToWalk = false;
					}
					if (session.level.getBlock(newPersonGrid).isFallable()
							&& session.level.getBlock(newPersonGrid.offset(0, 1))
									.isFallable()) {
						lodePerson.setFalling(true);
						lodePerson.setWalking(false);
						lodePerson.setOnLadder(false);
					}
				}
			}
			if (clirection == -1) {
				if (newPersonGrid.getY() == -1
						&& newPersonGrid.getX() == session.level.getExitColumn()) {
					if (lodePerson instanceof Player) {
						winningBehaviour();
					} else {
						lodePerson.setPosition(personPosX, personPosY);
						return;
					}
				} else if (session.level.getBlock(newPersonGrid).isSolid()
						&& !lodePerson.wantsToWalk) {
					personPosY = personGrid.getPixelY();
					lodePerson.stop();
				}
				if (!newPersonGrid.equals(personGrid)) {
					if (!session.level.getBlock(personGrid).isLadder()
							&& !lodePerson.wantsToWalk
							&& !(session.level.getBlock(personGrid).isRod() && session.level
									.getBlock(newPersonGrid).isLadder())) {
						personPosY = personGrid.getPixelY();
						lodePerson.stop();
						lodePerson.setOnLadder(false);
						lodePerson.setWalking(true);
					}
					if (lodePerson.wantsToWalk) {
						lodePerson.stopClimbing();
						personPosY = personGrid.getPixelY();
						lodePerson.setOnLadder(false);
						lodePerson.setWalking(true);
						lodePerson.wantsToWalk = false;
					}
				}
			}
			if (clirection == 0) {
				if (lodePerson.wantsToWalk) {
					if (faceBlock.getY() > personGrid.getY()) {
						lodePerson.setWantedClimbingDirection(1);
					}
					if (faceBlock.getY() == personGrid.getY()) {
						if (personGrid.getPixelY() == (int) lodePerson
								.getPositionY()) {
							lodePerson.stopClimbing();
							lodePerson.setOnLadder(false);
							lodePerson.setWalking(true);
							lodePerson.wantsToWalk = false;
						} else {
							lodePerson.setWantedClimbingDirection(-1);
						}
					}
				}
			}
		}

		// Setzt die neue, legale Position des Spielers um.
		lodePerson.setPosition(personPosX, personPosY);
	}

	private void winningBehaviour() {
		session.castLevelPassed();
	}

	@Override
	protected void update() {
		session.player.translateInput();
		interpretLodePersonInput(session.player);
		checkForDeath(session.player);

		for (Enemie enemie : session.enemies) {
			enemie.translateInput();
			interpretLodePersonInput(enemie);
			checkForDeath(enemie);
			checkForBlockedHoles(enemie);
		}

		fillUpHoles();
		checkTreasures();
	}
}