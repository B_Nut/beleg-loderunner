package sessionLodeRunner;

import java.awt.event.KeyListener;

/**
 * Der menschliche Spieler. Er benoetigt den KeyListener, der im Frame eingebaut ist.
 */
public class Player extends LodePerson{
	
	private PlayerInput uInput;
	
	public Player(GridAddress gA) {
		super(gA);
		uInput = new PlayerInput();
		setLodePersonInput(uInput);
	}
	
	public KeyListener getKeyListener(){
		return uInput;
	};
}
