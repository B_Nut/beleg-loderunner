package sessionLodeRunner;

import java.util.ArrayList;

/**
 * Frei nach dem Observer-Pattern gestaltete Klasse.
 */
public class GameSessionEventTrigger {
	private ArrayList<GameSessionListener> listener;
	
	public GameSessionEventTrigger(){
		listener = new ArrayList<GameSessionListener>();
	}
	public void addGameSessionListener(GameSessionListener observer){
		listener.add(observer);
	}
	public void removeGameSessionListener(GameSessionListener observer){
		listener.remove(observer);
	}
	
	public void castPlayerDied(){
		for(GameSessionListener gSL : listener){
			gSL.playerDied();
		}
	}
	public void castLevelPassed(){
		for(GameSessionListener gSL : listener){
			gSL.levelPassed();
		}
	}
}