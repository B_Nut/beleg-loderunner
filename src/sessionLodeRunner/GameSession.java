package sessionLodeRunner;

/**
 * Beherbergt alle Informationen, des laufenden Spiels.
 */
public class GameSession extends GameSessionEventTrigger{
	
	public Level level;
	public Player player;
	public Enemie[] enemies;
	
	public GameWindow window;
	
	private Thread sessionThread;
	private Thread renderThread;
	
	private GameLogic gameLogic;
	
	private VariableTimestepRunnable renderLogic = new VariableTimestepRunnable(65){
		@Override
		protected void update() {
			window.updateWindow();
		}
	};
	
	/**
	 * @param levelPath Der zur Anwendung relative Pfad zum Level
	 */
	public GameSession(String levelPath){
		level = new Level(levelPath);
		player = new Player(level.getPlayerSpawn());
		enemies = new Enemie[level.getEnemieSpawns().length];
		for(int i = 0; i < level.getEnemieSpawns().length; i++){
			enemies[i] = new Enemie(level.getEnemieSpawns()[i]);
		}
		
		window = new GameWindow(this);
		window.addKeyListener(player.getKeyListener());
		gameLogic = new GameLogic(this, 120);
	}
	
	/**
	 * Initialisiert die Threads und startet sie.
	 */
	public void startSession(){
		sessionThread = new Thread(gameLogic);
		sessionThread.setName("GameLogic");
		renderThread = new Thread(renderLogic);
		renderThread.setName("RenderLogic");
	    sessionThread.start();
	    renderThread.start();
	}
	
	/**
	 * Bittet dem Spiel- und Renderthread, sich zu beenden.
	 */
	public void end(){
		gameLogic.end();
		renderLogic.end();
	}
	
	/**
	 * Pausiert das Spiel.
	 */
	public void pause(){
		gameLogic.pause();
		renderLogic.pause();
	}
	
	/**
	 * Faehrt mit dem Spiel fort.
	 */
	public void resume(){
		gameLogic.resume();
		renderLogic.resume();
	}
}
