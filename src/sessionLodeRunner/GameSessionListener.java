package sessionLodeRunner;

/**
 * Wer dieses Interface einbindet, kann sich bei einem GameSessionEventTrigger anmelden und wird ueber Ereignisse informiert. <br>
 */
public interface GameSessionListener {
	public void playerDied();
	public void levelPassed();
}
