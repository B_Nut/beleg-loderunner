package sessionLodeRunner;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

/**
 * Ein KeyListener, der dazu in der Lage ist, mehrfache Eingaben zu
 * stapeln und nur eine eindeutige Eingabe zurueckzugeben.
 */
public class PlayerInput implements KeyListener, LodePersonInput{

	private ArrayList<Integer> keyOrder;

	public PlayerInput() {
		keyOrder = new ArrayList<Integer>();
		keyOrder.add(0);
	}
	
	@Override
	public int getInput() {
		synchronized (keyOrder) {
			return keyOrder.get(keyOrder.size() - 1);
		}
	}

	private void stackKey(int customKeyCode) {
		synchronized (keyOrder) {
			// Um im vorhinein Fehler nicht erkannter KeyReleased zu vermeiden,
			// loesche ich eventuelle doppelte Eingaben vorher.
			keyOrder.remove(new Integer(customKeyCode));
			keyOrder.add(customKeyCode);
		}
	}

	private void unstackKey(int customKeyCode) {
		synchronized (keyOrder) {
			keyOrder.remove(new Integer(customKeyCode));
		}
	}

	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			stackKey(LodePerson.CKC_LEFT);
			break;
		case KeyEvent.VK_RIGHT:
			stackKey(LodePerson.CKC_RIGHT);
			break;
		case KeyEvent.VK_UP:
			stackKey(LodePerson.CKC_UP);
			break;
		case KeyEvent.VK_DOWN:
			stackKey(LodePerson.CKC_DOWN);
			break;
		case KeyEvent.VK_X:
			stackKey(LodePerson.CKC_DIG_LEFT);
			break;
		case KeyEvent.VK_C:
			stackKey(LodePerson.CKC_DIG_RIGHT);
			break;
		}

	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_LEFT:
			unstackKey(LodePerson.CKC_LEFT);
			break;
		case KeyEvent.VK_RIGHT:
			unstackKey(LodePerson.CKC_RIGHT);
			break;
		case KeyEvent.VK_UP:
			unstackKey(LodePerson.CKC_UP);
			break;
		case KeyEvent.VK_DOWN:
			unstackKey(LodePerson.CKC_DOWN);
			break;
		case KeyEvent.VK_X:
			unstackKey(LodePerson.CKC_DIG_LEFT);
			break;
		case KeyEvent.VK_C:
			unstackKey(LodePerson.CKC_DIG_RIGHT);
			break;
		}
	}
	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
	}
}