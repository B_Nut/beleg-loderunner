package sessionLodeRunner;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * Repraesentiert ein Level im Spiel LodeRunner.
 */
public class Level {
	private String levelPath;
	
	private GameBlock[][] grid;
	
	private int numberOfTreasures;
	
	private TextureReferrer textureReferrer;
	
	private String levelName;
	
	private GridAddress playerSpawn;
	private GridAddress[] enemieSpawns;
	
	private int exitColumn;
	
	public Level(String levelPath){
		this.levelPath = levelPath;
		numberOfTreasures = 0;
		try {
			parseLevel(readFile(levelPath));
		} catch (IOException e) {
			System.out.println("Error while reading the file:");
			e.printStackTrace();
			System.exit(-1);
		}
		textureReferrer.assignAllTextures(this);
	}
	
	public String getName(){ return levelName; }
	public String getLevelPath(){ return levelPath; }
	
	public int getHeight(){
		return grid[0].length;
	}
	
	public int getWidth(){
		return grid.length;
	}
	
	public int getNumberOfTreasures(){
		return numberOfTreasures;
	}
	
	/**
	 * Gibt die Instanz von GameBlock zurueck, die an der gegebenen Adresse gefunden wurde.
	 * @return null, wenn die Adresse außerhalb des Levels ist.
	 */
	public GameBlock getBlock(GridAddress gA){
		int x = gA.getX();
		int y = gA.getY();
		
		if( x >= 0 && x < this.getWidth() && y >= 0 && y < this.getHeight()){
			return grid[x][y];
		}else{
			return null;
		}
	}
	
	public GridAddress getPlayerSpawn(){
		return playerSpawn;
	}
	
	public GridAddress[] getEnemieSpawns(){
		return enemieSpawns;
	}
	public int getExitColumn(){
		return exitColumn;
	}
	
	public void reAssign(GridAddress gA, byte id){
		getBlock(gA).changeID(id);
		reAssignTexture(gA);
	}
	public void reAssignTexture(GridAddress gA){
		textureReferrer.assignTexture(this, gA);
	}
	
	public void eliminateTreasure(GridAddress gA){
		if(getBlock(gA).getID() == 1){
			reAssign(gA, GameBlock.ID_EMPTY);
			numberOfTreasures--;
		}
	}
	
	public void justTakeTreasure(GridAddress gA){
		if(getBlock(gA).getID() == 1){
			reAssign(gA, GameBlock.ID_EMPTY);
		}
	}
	
	public void openLevelExit(){
		GridAddress exitSource = new GridAddress(exitColumn,0);
		reAssign(exitSource, GameBlock.ID_LADDER);
		for(int i = 1; getBlock(exitSource.offset(0, i)).isFallable(); i++){
			reAssign(exitSource.offset(0, i), GameBlock.ID_LADDER);
		}
	}
	
	private static String readFile(String path) throws IOException{
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, Charset.defaultCharset());
	}
	
	/**
	 * Friemelt den String auseinander und entnimmt die entsprechenden Variablen. JA ES FRIEMELT! 
	 * Definiert ist das Protokoll in "portfolio/LevelVorgaben".
	 * @param encodedLevel Der String, welcher das Level repraesentiert. 
	 */
	private void parseLevel(String encodedLevel){		
		//Remove unwanted and invisible components.
		encodedLevel = encodedLevel.replaceAll("\n", "");
		encodedLevel = encodedLevel.replaceAll("\r", "");
		
		//Split the fields at the semicolon.
		String[] categories = encodedLevel.split(";");
		
		//Split first field...
		String[] levelInfo = categories[0].split(",");
		//Assign Infos.
		levelName = levelInfo[0];
		String mod = levelInfo[1];

		//Decides the textures assigned to all GameBlocks.
		textureReferrer = new TextureReferrer(mod);
		
		//Split second field.
		String[] spawns = categories[1].split(",");
		
		//First two integers are the position of the player.
		playerSpawn = new GridAddress(Integer.parseInt(spawns[0]),Integer.parseInt(spawns[1]));
		
		
		//Iterate through the spawns of the enemies.
		if(spawns.length % 2 != 0 && spawns.length >= 2){
			System.out.println("Please check Spawns. The number of values must be more than two and even!");
			System.exit(-1);
		}
		enemieSpawns = new GridAddress[(spawns.length-2)/2];
		for(int i = 0; i < enemieSpawns.length;i++){
			enemieSpawns[i] = new GridAddress(Integer.parseInt(spawns[2*i+2]),Integer.parseInt(spawns[2*i+3]));
		}
		//The Column where the exit will appear is defined in the third field.
		exitColumn = Integer.parseInt(categories[2].split(",")[2]);
		
		//Split the third field containing the ID's of the Blocks into their lines.
		String[] levelContent = categories[3].split(",");
		
		//Iterate through the 2D-Array line per line.
		grid = new GameBlock[levelContent[0].length()][levelContent.length];
		for(int yi = 0; yi < getHeight(); yi++){
			for(int xi = 0; xi < getWidth(); xi++){
				//Initialize the GameBlock and assign the given ID.
				byte id = Byte.parseByte("" + levelContent[yi].charAt(xi));
				grid[xi][yi] = new GameBlock(id);
				if(id == 1){
					numberOfTreasures++;
				}
			}
		}
	}
}
