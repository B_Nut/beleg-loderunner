package sessionLodeRunner;

import java.awt.Graphics;
import java.awt.image.BufferedImage;

/**
 * Diese Klasse kann das Aussehen der Bloecke, mithilfe der Klassen {@link TextureContainer} und {@link GameBlock} feststellen.
 */
public class TextureReferrer {
	private TextureContainer textures;
	
	/**
	 * Initialisiert den TextureContainer.
	 * @param mod muss der Name eines Unterordners in {@value sessionLodeRunner.TextureContainer#BASE_PATH} sein.
	 */
	public TextureReferrer(String mod){
		//initialize all Textures of a certain texture pack (mod).
		textures = new TextureContainer(mod);
	}
	
	
	public void assignAllTextures(Level level){
		//Iterate through the 2D-GridArray of the level.
		for(int yi = 0; yi < level.getHeight(); yi++){
			for(int xi = 0; xi < level.getWidth(); xi++){
				assignTexture(level,new GridAddress(xi,yi));
			}
		}
	}
	
	public void assignTexture(Level level, GridAddress gA){
		GameBlock gB = level.getBlock(gA);
		int iD = (int)gB.getID();
		//Assign the textures by given ID.
		if(iD == GameBlock.ID_EMPTY){
			gB.setImage(textures.empty);
		}
		if(iD == GameBlock.ID_TREASURE){
			gB.setImage(textures.treasure);
		}
		if(iD == GameBlock.ID_BRICK){
			gB.setImage(textures.brick);
		}
		if(iD == GameBlock.ID_CLAY || iD == GameBlock.ID_HOLE){
			GameBlock uB = level.getBlock(gA.offset(0, -1));
			double ratioLeft = gB.getClayHoleConversionFactor(); //Between 0 and 1
			if(ratioLeft == 1){
				gB.setImage(textures.clay);
				if(uB.isWithinAir()){
					uB.setForeground(textures.grassTop);
					gB.setForeground(textures.grassBot);
				}
			}else{
				//Determine the foreground.
				byte lBID = level.getBlock(gA.offset(-1, 0)).getID();
				byte rBID = level.getBlock(gA.offset(1, 0)).getID();
				if(lBID == GameBlock.ID_CLAY && rBID == GameBlock.ID_CLAY){
					gB.setForeground(merge(textures.holeL,textures.holeR));
				}else if(lBID == GameBlock.ID_CLAY){
					gB.setForeground(textures.holeL);
				}else if(rBID == GameBlock.ID_CLAY){
					gB.setForeground(textures.holeR);
				}else{
					gB.setForeground(textures.empty);
				}
				
				//Determine the Diggingpic.
				if(ratioLeft > 0.6){
					uB.setForeground(textures.empty);
					gB.setImage(textures.clayDigged1);
				}else if(ratioLeft > 0.3){
					uB.setForeground(textures.empty);
					gB.setImage(textures.clayDigged2);
				}else{
					uB.setForeground(textures.empty);
					gB.setImage(textures.empty);
				}
			}
		}
		if(iD == GameBlock.ID_LADDER){
			gB.setImage(textures.ladder);
		}
		if(iD == GameBlock.ID_ROD){
			gB.setImage(textures.rod);
		}
		gB.setImage(merge(textures.background, gB.getImage()));
	}
	
	private BufferedImage merge(BufferedImage background, BufferedImage foreground){
		BufferedImage merged = new BufferedImage(background.getWidth(),background.getHeight(), BufferedImage.TYPE_INT_ARGB);
		Graphics g = merged.getGraphics();
		g.drawImage(background, 0, 0, null);
		g.drawImage(foreground, 0, 0, null);
		g.dispose();
		return merged;
	}
}