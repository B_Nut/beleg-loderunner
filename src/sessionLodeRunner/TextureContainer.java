package sessionLodeRunner;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
/**
 * Initialisiert die Bilder eines entsprechenden Mods.
 * @see sessionLodeRunner.TextureReferrer
 */
public class TextureContainer {
	
	/**
	 * Der relative Pfad zu den Texturen, hier sind die "TexturMods" gespeichert.
	 * Dieser Pfad ist: {@value #BASE_PATH}
	 */
	public static final String BASE_PATH = "graphics/block/";
	
	public final BufferedImage background;
	
	public final BufferedImage empty;
	public final BufferedImage treasure;
	public final BufferedImage brick;
	public final BufferedImage clay;
	public final BufferedImage clayDigged1;
	public final BufferedImage clayDigged2;
	public final BufferedImage holeL;
	public final BufferedImage holeR;
	public final BufferedImage ladder;
	public final BufferedImage rod;
	
	public final BufferedImage grassTop;
	public final BufferedImage grassBot;
	
	public static final BufferedImage PLAYER_IDLE = getBufferedImage("graphics/animation/char2idlev2.gif");
	public static final BufferedImage ENEMIE_IDLE = getBufferedImage("graphics/animation/BadBoy.png");
	
	/**
	 * Initialisiert die Texturen mit der entsprechenden Mod.
	 * @param mod muss einem Ordnernamen in {@value #BASE_PATH} entsprechen.
	 */
	public TextureContainer(String mod){
		
		String path = BASE_PATH + mod + "/";
		
		background	= getBufferedImage(path + "Background.png");
		empty		= getBufferedImage(path + "Empty.png");
		treasure	= getBufferedImage(path + "Treasure.png");
		brick		= getBufferedImage(path + "Brick.png");
		clay		= getBufferedImage(path + "Clay.png");
		clayDigged1	= getBufferedImage(path + "Clay(DiggedOnce).png");
		clayDigged2	= getBufferedImage(path + "Clay(DiggedTwice).png");
		holeL		= getBufferedImage(path + "Hole(L).png");
		holeR		= getBufferedImage(path + "Hole(R).png");
		ladder		= getBufferedImage(path + "Ladder.png");
		rod			= getBufferedImage(path + "Rod.png");
		grassTop	= getBufferedImage(path + "GrasTop.png");
		grassBot	= getBufferedImage(path + "GrasBot.png");
		
	}
	
	/**
	 * Laedt ein Bild aus einem gegeben Pfad.
	 * @param path der Pfad zum Bild.
	 * @return Eine Instanz von {@link BufferedImage}, oder null, wenn nicht vorhanden.
	 */
	private static BufferedImage getBufferedImage(String path){
		try {
			return ImageIO.read(new File(path));
		} catch (IOException e) {
			System.out.println(e.getMessage() + " : " + path);
			return (BufferedImage)null;
		}
	}
}