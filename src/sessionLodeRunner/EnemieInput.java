package sessionLodeRunner;

import java.util.Random;

/**
 * Erzeugt jede Sekunde eine neue Eingabe auf zufälliger Basis.
 */
public class EnemieInput implements LodePersonInput{
	private Random randomGenerator;
	private long lastInputTime;
	
	private int lastInput;
	
	public EnemieInput(){
		randomGenerator = new Random();
		lastInputTime = System.currentTimeMillis();
		lastInput = 0;
	}
	
	@Override
	public int getInput() {
		if(System.currentTimeMillis() - lastInputTime > 1000){
			lastInputTime = System.currentTimeMillis();
			//Siehe die CustomKeyCode-Variablen in LodePerson
			lastInput = Math.abs(randomGenerator.nextInt() % 5);
		}
		return lastInput;
	}

}
